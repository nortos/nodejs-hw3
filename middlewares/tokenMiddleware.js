const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');

module.exports.checkToken = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    res.status(401).json({message: 'There is no authorization header'});
  }

  const token = header.split(' ')[1];
  if (!token) {
    res.status(401).json({message: 'There is no token'});
  }

  req.user = jwt.verify(token, JWT_SECRET);
  next();
};
