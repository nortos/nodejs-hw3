require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const app = express();

const YAML = require('yamljs');
const swaggerUI = require('swagger-ui-express');
const swaggerDocument = YAML.load('./swagger.yaml');

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');

const mongoLink = process.env.MONGO_URL ||
  'mongodb+srv://Nordtonito:VitNes@hw2.kpbxv.mongodb.net/hw3db?retryWrites=true&w=majority';
const port = process.env.PORT || 8080;

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);
app.use((err, req, res) => {
  res.status(500).json({message: err.message});
});

const run = async () => {
  try {
    await mongoose.connect(mongoLink, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    app.listen(port, () => {
      console.log(`The server is listened on the port ${port}`);
    });
  } catch (err) {
    console.log(err.message);
  }
};

run();
