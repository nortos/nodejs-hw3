const express = require('express');
const router = new express.Router();

const {mainWrapper} = require('../helpers');
const {checkToken} = require('../middlewares/tokenMiddleware');
const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require('../controllers/usersController');

router.get('/', mainWrapper(checkToken), mainWrapper(getUser));
router.delete('/', mainWrapper(checkToken), mainWrapper(deleteUser));
// eslint-disable-next-line max-len
router.patch('/password', mainWrapper(checkToken), mainWrapper(changeUserPassword));

module.exports = router;
