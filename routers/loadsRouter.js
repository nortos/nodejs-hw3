const express = require('express');
const router = new express.Router();

const {mainWrapper} = require('../helpers');
const {checkToken} = require('../middlewares/tokenMiddleware');
const {
  getLoads,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
} = require('../controllers/loadsController');

router.get('/', mainWrapper(checkToken), mainWrapper(getLoads));
router.post('/', mainWrapper(checkToken), mainWrapper(addLoad));
router.get('/active', mainWrapper(checkToken), mainWrapper(getActiveLoad));
// eslint-disable-next-line max-len
router.patch('/active/state', mainWrapper(checkToken), mainWrapper(nextLoadState));
router.get('/:id', mainWrapper(checkToken), mainWrapper(getLoadById));
router.put('/:id', mainWrapper(checkToken), mainWrapper(updateLoadById));
router.delete('/:id', mainWrapper(checkToken), mainWrapper(deleteLoadById));
router.post('/:id/post', mainWrapper(checkToken), mainWrapper(postLoadById));
// eslint-disable-next-line max-len
router.get('/:id/shipping_info', mainWrapper(checkToken), mainWrapper(getLoadById));

module.exports = router;
