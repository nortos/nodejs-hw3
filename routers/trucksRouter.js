const express = require('express');
const router = new express.Router();

const {mainWrapper} = require('../helpers');
const {checkToken} = require('../middlewares/tokenMiddleware');
const {
  addTruck,
  getTrucks,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../controllers/trucksController');

router.get('/', mainWrapper(checkToken), mainWrapper(getTrucks));
router.post('/', mainWrapper(checkToken), mainWrapper(addTruck));
router.get('/:id', mainWrapper(checkToken), mainWrapper(getTruckById));
router.put('/:id', mainWrapper(checkToken), mainWrapper(updateTruckById));
router.delete('/:id', mainWrapper(checkToken), mainWrapper(deleteTruckById));
// eslint-disable-next-line max-len
router.post('/:id/assign', mainWrapper(checkToken), mainWrapper(assignTruckById));

module.exports = router;
