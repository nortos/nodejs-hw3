const express = require('express');
const router = new express.Router();

const {mainWrapper} = require('../helpers');
const {validateRegistration} = require('../middlewares/validationMiddleware');
const {
  register,
  login,
  forgotPassword,
} = require('../controllers/authController');

// eslint-disable-next-line max-len
router.post('/register', mainWrapper(validateRegistration), mainWrapper(register));
router.post('/login', mainWrapper(login));
router.post('/forgot_password', mainWrapper(forgotPassword));

module.exports = router;
