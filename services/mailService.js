const nodemailer = require('nodemailer');

const smtpConfig = {
  host: 'smtp.mailtrap.io',
  port: 2525,
  auth: {
    user: '7b9bd01cf98bf3',
    pass: 'bb5f84bd7fe36c',
  },
};

async function sendPassword(mailOptions) {
  const transporter = nodemailer.createTransport(smtpConfig);

  return await transporter.sendMail(mailOptions);
}

module.exports = {sendPassword};
