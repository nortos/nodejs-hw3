const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: {
    type: mongoose.Types.ObjectId,
    default: null,
    ref: 'User',
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    default: 'IS',
    enum: ['IS', 'OL'],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
