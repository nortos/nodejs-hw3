const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');
const {Load} = require('../models/loadModel');

const getLoads = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  // eslint-disable-next-line no-unused-vars
  const {status, limit = '0', offset = '0'} = req.query;
  const userRole = user.role;
  let loads;

  if (userRole === 'SHIPPER') {
    loads = await Load.find({created_by: user._id}, {__v: 0},
        {
          skip: parseInt(offset),
          limit: limit > 100 ? 10 : parseInt(limit),
        });
  } else {
    loads = await Load.find({assigned_to: user._id});
  }

  if (loads) {
    res.json({loads});
  } else {
    res.status(400).json({message: 'There are no loads'});
  }
};

const addLoad = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (user.role !== 'SHIPPER') {
    res.status(400).json({message: 'The user should be a shipper'});
  }

  const load = new Load({...req.body, created_by: user._id});

  await load.save();
  res.json({message: 'Load created successfully'});
};

const getActiveLoad = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  const userRole = user.role;

  if (userRole === 'DRIVER') {
    // eslint-disable-next-line max-len
    const load = await Load.findOne({assigned_to: req.user._id, status: 'ASSIGNED'});
    res.json({load});
  } else {
    res.status(400).json({message: 'The user is not a driver'});
  }
};

const nextLoadState = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  const userRole = user.role;

  if (userRole === 'DRIVER') {
    // eslint-disable-next-line max-len
    const load = await Load.findOne({assigned_to: req.user._id, status: 'ASSIGNED'}, {__v: 0});

    if (!load) {
      res.status(400).json({message: 'There are no loads'});
    }

    if (load.status !== 'ASSIGNED') {
      res.status(400).json({message: 'You can\'t change the state now'});
    }

    let newState;
    switch (load.state) {
      case 'En route to Pick Up':
        newState = 'Arrived to Pick Up';
        break;
      case 'Arrived to Pick Up':
        newState = 'En route to delivery';
        break;
      case 'En route to delivery':
        newState = 'Arrived to delivery';
        load.status = 'SHIPPED';
        break;
    }

    load.state = newState;
    await load.save();

    res.json({message: `Load state changed`});
  } else {
    res.status(400).json({message: 'User is not a driver'});
  }
};

const getLoadById = async (req, res) => {
  // eslint-disable-next-line max-len
  const load = await Load.findOne({created_by: req.user._id, _id: req.params.id});

  if (!load) {
    res.status(400).json({message: 'There is no load with this id'});
  }

  res.json({load});
};

const updateLoadById = async (req, res) => {
  // eslint-disable-next-line max-len
  const load = await Load.findOne({created_by: req.user._id, _id: req.params.id}, {__v: 0});
  if (!load) {
    res.status(400).json({message: 'There is no load with this id'});
  }
  await Load.updateOne(load, req.body);
  res.json({message: 'Load details changed successfully'});
};

const deleteLoadById = async (req, res) => {
  // eslint-disable-next-line max-len
  const load = await Load.findOne({created_by: req.user._id, _id: req.params.id});
  if (!load) {
    res.status(400).json({message: 'There is no load with this id'});
  }
  await Load.deleteOne(load);
  res.json({message: 'Load deleted successfully'});
};

const postLoadById = async (req, res) => {
  const load = await Load.findById(req.params.id);

  if (!load) {
    res.status(400).json({message: 'There is no load with this id'});
  }

  if (load.status !== 'NEW') {
    res.status(400).json({message: 'The status of load should be new'});
  }

  await Load.findByIdAndUpdate({_id: req.params.id}, {status: 'POSTED'});
  // eslint-disable-next-line max-len
  const truck = await Truck.where('assigned_to').ne(null).where('status').equals('IS').findOne();

  if (truck) {
    load.assigned_to = truck.assigned_to;
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    // eslint-disable-next-line max-len
    load.logs.push({message: `Load assigned to driver with id ${load.assigned_to}`, time: Date.now()});

    await load.save();
    await Truck.findByIdAndUpdate(truck._id, {status: 'OL'});
    res.json({message: 'Load posted successfully', driver_found: true});
  } else {
    load.status = 'NEW';
    await load.save();
    res.json({message: 'Driver was not found', driver_found: false});
  }
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
};
