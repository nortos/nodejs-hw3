const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');

const getUser = async (req, res) => {
  // eslint-disable-next-line max-len
  const user = await User.findOne({_id: req.user._id}, {password: 0, role: 0, __v: 0});

  if (!user) {
    res.status(400).json({message: `No user was found!`});
  }

  res.json({user});
};

const deleteUser = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    res.status(400).json({message: 'No user found!'});
  }

  await User.deleteOne({_id: req.user._id});
  res.json({message: 'Profile deleted successfully'});
};

const changeUserPassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;

  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    res.status(400).json({message: 'No user found!'});
  }

  if (oldPassword && newPassword) {
    if (!(await bcrypt.compare(oldPassword, user.password))) {
      res.status(400).json({message: `Wrong password!`});
    } else {
      if (newPassword !== oldPassword) {
        user.password = await bcrypt.hash(newPassword, 10);
        await user.save();
        res.json({message: 'Password changed successfully'});
      } else {
        res.status(400).json({
          message: `Password should be different from old one!`,
        });
      }
    }
  } else {
    res.status(400).json({
      message: 'Old and new passwords fields are required!',
    });
  }
};

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword,
};
