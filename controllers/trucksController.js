const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

const getTrucks = async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id}, {__v: 0});

  res.json({trucks});
};

const addTruck = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (user.role !== 'DRIVER') {
    res.status(400).json({message: 'The user should be a driver'});
  }

  const truck = new Truck({
    created_by: user._id,
    type: req.body.type,
  });

  await truck.save();
  res.json({message: 'Truck created successfully'});
};

const getTruckById = async (req, res) => {
  const truck = await Truck.findOne(
      {created_by: req.user._id, _id: req.params.id}, {__v: 0},
  );

  if (!truck) {
    res.status(400).json({message: 'There is no truck with this id'});
  }

  res.json({truck});
};

const updateTruckById = async (req, res) => {
  const truck = await Truck.findOne(
      {created_by: req.user._id, _id: req.params.id}, {__v: 0},
  );
  if (!truck) {
    res.status(400).json({message: 'There is no truck with this id'});
  }

  await Truck.updateOne(truck, req.body);
  res.json({message: 'Truck details changed successfully'});
};

const deleteTruckById = async (req, res) => {
  const truck = await Truck.findOne(
      {created_by: req.user._id, _id: req.params.id}, {__v: 0},
  );
  if (!truck) {
    res.status(400).json({message: 'There is no truck with this id'});
  }

  await Truck.deleteOne(truck);
  res.json({message: 'Truck deleted successfully'});
};

const assignTruckById = async (req, res) => {
  const truck = await Truck.findOne(
      {created_by: req.user._id, _id: req.params.id}, {__v: 0},
  );
  if (!truck) {
    res.status(400).json({message: 'There is no truck with this id'});
  }

  await Truck.updateOne(truck, {assigned_to: req.user._id});
  res.json({message: 'Truck assigned successfully'});
};

module.exports = {
  addTruck,
  getTrucks,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
