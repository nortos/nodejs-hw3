const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const otpGenerator = require('otp-generator');

const {JWT_SECRET} = require('../config');
const {sendPassword} = require('../services/mailService');

const {User} = require('../models/userModel');

const register = async (req, res) => {
  const {email, password, role} = req.body;

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();

  res.json({message: 'User created successfully'});
};

const login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    res.status(400).json({message: `There is no such user`});
  }
  if (!(await bcrypt.compare(password, user.password))) {
    res.status(400).json({message: `Incorrect password`});
  }
  const token = jwt.sign(
      {
        _id: user._id,
        email: user.email,
        createdDate: user.createdDate,
      },
      JWT_SECRET,
  );
  res.json({message: 'Success', jwt_token: token});
};

const forgotPassword = async (req, res) => {
  const {email} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      message: `There is no user with this email`,
    });
  }

  const recoveryKey = otpGenerator.generate(6, {
    digits: true,
    upperCase: true,
    specialChars: true,
    alphabets: true,
  });

  await sendPassword({
    from: 'transportation.service.help@gmail.com',
    to: email,
    subject: 'Your new password',
    text: 'Please confirm your email',
    html: `Here is your new password - ${recoveryKey}`,
  });

  const password = await bcrypt.hash(recoveryKey, 10);

  await User.updateOne({email}, {password});

  res.json({
    message: 'New password sent to your email address',
  });
};

module.exports = {
  register,
  login,
  forgotPassword,
};
